FROM php:8.2-cli

ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/

ENV IPE_GD_WITHOUTAVIF=0

RUN chmod +x /usr/local/bin/install-php-extensions && install-php-extensions \
    @composer \
    bcmath \
    gd \
    intl \
    mcrypt \
    pdo_mysql \
    pdo_pgsql \
    redis \
    xdebug \
    xsl \
    zip

RUN apt-get update && apt-get install openssh-client -qq
